#!/usr/bin/env bash

PATH_OLD=$PATH
DATADIR=/mnt/data/sort/data

function start_server {

	data=$1
	bins=$2
	odir=$3
	conf=$4

	export PATH=$bins/bin:$PATH_OLD

	mkdir $odir

	cp $conf $data/postgresql.conf

	killall postgres > /dev/null 2>&1
	sleep 10

	pg_ctl -D $data -l $odir/pg.log -w start > $odir/start.log 2>&1

	if [ "$?" != "0" ]; then
		echo "ERROR: starting server failed"
		exit
	fi

	pg_config > $odir/config.log 2>&1

	psql postgres -c "SELECT * FROM pg_settings" > $odir/setting.log 2>&1

	ps ax > $odir/ps.log 2>&1
}

function stop_server {

	data=$1
	odir=$2

	pg_ctl -D $data -w stop > $odir/stop.log 2>&1

}

function check_stop {

        if [ -f "stop" ]; then
                exit
        fi

}

function git_push {

	gzip $1/$2/pg.log

	git add $1/$2
	git add run.log
        git commit -m "$1 $2"
        git push origin master
}


for nrows in 10000 100000 1000000; do

	check_stop

	mkdir $nrows


	# master with default replacement_sort_tuples value

	start_server $DATADIR /var/lib/postgresql/pg-master $nrows/master master.conf

        ./sort-bench.sh test $nrows $nrows/master > $nrows/master/bench.log 2>&1

	stop_server $DATADIR $nrows/master

	git_push $nrows master

	check_stop


	# patched with entirely removed replacement sort

	start_server $DATADIR /var/lib/postgresql/pg-patched $nrows/patched master.conf

       	./sort-bench.sh test $nrows $nrows/patched > $nrows/patched/bench.log 2>&1

	stop_server $DATADIR $nrows/patched

	git_push $nrows patched

	check_stop

done
